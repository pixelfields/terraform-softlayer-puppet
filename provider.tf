
provider "softlayer" {
  username = "${var.sl_username}"
  api_key = "${var.sl_api_key}"
}
