# README #

This boilerplate is using [Terraform](https://www.terraform.io/) to provision Puppet master on [Softlayer](http://www.softlayer.com/) cloud platform.

### Requirements ###

The only software requirement is **terraform v0.6.16**.

You also need an Softlayer account with privileges for managing resources.

### Running ###

Running terraform is very simple. 

* copy **terraform.tfvars.example** file to ** terraform.tfvars**
* replace example values with your own
* run 
```
#!bash

terraform plan
```
  to see all changes that will be made before actually applying by looking at the plan. Then apply changes with

```
#!bash

terraform apply
```

