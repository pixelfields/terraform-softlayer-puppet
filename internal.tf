# This will provision base test instance and connect it to puppet master to do initial setup

resource "softlayer_virtual_guest" "int" {

  depends_on = [
    "null_resource.puppet-master-configure",
  ]

  name = "int"
  domain = "${var.domain}"
  ssh_keys = ["${softlayer_ssh_key.default_key.id}"]
  image = "DEBIAN_8_64"
  region = "ams01"
  public_network_speed = 100
  hourly_billing = false
  private_network_only = false
  cpu = 4
  ram = 4096
  disks = [100]
  dedicated_acct_host_only = false
  local_disk = true

}

resource "null_resource" "int-puppet-agent-bootstrap" {

  provisioner "file" {
    source = "files/puppet-agent-bootstrap-debian.sh"
    destination = "/tmp/puppet-agent-bootstrap-debian.sh"

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "apt-get install -y make gcc python-dev python-pip",
      "pip install awscli",
      "aws configure set default.region eu-west-1",
      "aws configure set default.output table",
      "aws configure set default.aws_access_key_id ${var.aws_access_key}",
      "aws configure set default.aws_secret_access_key ${var.aws_secret_access_key}",
      "mkdir -p /var/lib/puppet/.aws",
      "cp /root/.aws/credentials /var/lib/puppet/.aws/credentials",
      "cp /root/.aws/config /var/lib/puppet/.aws/config"
    ]

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"${softlayer_virtual_guest.puppet-master-inst.ipv4_address}    pm.${var.domain} pm\" >> /etc/hosts"
    ]

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/puppet/int_puppet_agent.conf"
    destination = "/tmp/puppet_agent.conf"

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/cron/puppet_agent"
    destination = "/etc/cron.d/puppet_agent"

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/puppet-agent-bootstrap-debian.sh",
      "/tmp/puppet-agent-bootstrap-debian.sh"
    ]

    connection {
      host = "${softlayer_virtual_guest.int.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

}

output "int instance ip" {
  value = "${softlayer_virtual_guest.int.ipv4_address}"
}
