
# This will provision puppet master host

resource "softlayer_virtual_guest" "puppet-master-inst" {
  name = "pm"
  domain = "${var.domain}"
  ssh_keys = ["${softlayer_ssh_key.default_key.id}"]
  image = "DEBIAN_8_64"
  region = "ams01"
  public_network_speed = 100
  hourly_billing = false
  private_network_only = false
  cpu = 2
  ram = 1024
  disks = [25]
  dedicated_acct_host_only = false
  local_disk = true

}

resource "null_resource" "puppet-master-install" {

  provisioner "file" {
    source = "files/puppet-master-bootstrap-debian.sh"
    destination = "/tmp/puppet-master-bootstrap-debian.sh"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/puppet-master-bootstrap-debian.sh",
      "/tmp/puppet-master-bootstrap-debian.sh"
    ]

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }
}

resource "null_resource" "puppet-master-configure" {

  depends_on = [
    "null_resource.puppet-master-install"
  ]

  provisioner "remote-exec" {
    inline = [
      "mkdir /tmp/nginx",
      "mkdir /tmp/puppet",
      "mkdir /tmp/ssh",
      "mkdir /tmp/git"
    ]

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/nginx"
    destination = "/tmp"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/puppet"
    destination = "/tmp"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/ssh"
    destination = "/tmp"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "${var.sl_default_ssh_private_key_path}"
    destination = "/tmp/ssh/default_key"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/git/post-merge"
    destination = "/tmp/git/post-merge"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/puppet-master-nginx-passenger.sh"
    destination = "/tmp/puppet-master-nginx-passenger.sh"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "file" {
    source = "files/cron/puppet_repo"
    destination = "/etc/cron.d/puppet_repo"

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 600 /var/lib/puppet/.ssh/default_key",
      "chown -R puppet:puppet /var/lib/puppet/.ssh",
      "chmod +x /tmp/puppet-master-nginx-passenger.sh",
      "/tmp/puppet-master-nginx-passenger.sh"
    ]

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "aws configure set default.region eu-west-1",
      "aws configure set default.output table",
      "aws configure set default.aws_access_key_id ${var.aws_access_key}",
      "aws configure set default.aws_secret_access_key ${var.aws_secret_access_key}",
      "mkdir -p /var/lib/puppet/.aws",
      "cp /root/.aws/credentials /var/lib/puppet/.aws/credentials",
      "cp /root/.aws/config /var/lib/puppet/.aws/config",
      "chown -R puppet:puppet /var/lib/puppet/.aws/",
      "trocla set --password ${var.aws_access_key} aws_access_key_id plain",
      "trocla set --password ${var.aws_secret_access_key} aws_secret_access_key plain"
    ]

    connection {
      host = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
      user = "root"
      private_key = "${file(\"${var.sl_default_ssh_private_key_path}\")}"
    }
  }

}

output "puppet master ip" {
  value = "${softlayer_virtual_guest.puppet-master-inst.ipv4_address}"
}
