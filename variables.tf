variable "sl_username" {}
variable "sl_api_key" {}
variable "sl_default_ssh_key_path" {}
variable "sl_default_ssh_private_key_path" {}
variable "aws_access_key" {}
variable "aws_secret_access_key" {}
variable "domain" {}
