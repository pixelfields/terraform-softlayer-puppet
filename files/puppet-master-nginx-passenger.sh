#!/usr/bin/env bash

DISTRIB_CODENAME=$(lsb_release -c -s)

#-----------------------------------------------------------------------------------------------------------------------

echo "Installing apt-transport and ca-certificates ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install apt-transport-https ca-certificates >/dev/null

echo "Configuring Passenger apt repository ..."
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7 > /dev/null
echo deb https://oss-binaries.phusionpassenger.com/apt/passenger ${DISTRIB_CODENAME} main > /etc/apt/sources.list.d/passenger.list

apt-get update >/dev/null

echo "Installing Passenger and Nginx ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install nginx-extras passenger >/dev/null

mkdir -p /etc/puppet/rack/{public,tmp}

echo "Stopping nginx service ..."
service nginx stop > /dev/null

echo "Stopping puppet master service ..."
service puppetmaster stop > /dev/null

echo "Stopping puppet client service ..."
service puppet stop > /dev/null

echo "Setting timezone to Europe/Prague ..."
echo "Europe/Prague" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

echo "Running time synchronization ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ntpdate >/dev/null
ntpdate pool.ntp.org

echo "Installing ntp ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ntp >/dev/null

echo "Reconfiguring locales ..."
cp /etc/locale.gen /etc/locale.gen.orig
cat /dev/null > /etc/locale.gen
echo -e "en_US.UTF-8 UTF-8\ncs_CZ.UTF-8 UTF-8" > /etc/locale.gen
echo -e 'LANG="en_US.UTF-8"\nLANGUAGE="en_US:en"\n' > /etc/default/locale

echo "Copying puppet rack config file ..."
cp /tmp/puppet/config.ru /etc/puppet/rack/config.ru

echo "Copying puppet config file ..."
cp /etc/puppet/puppet.conf /etc/puppet/puppet.conf.orig
cp /tmp/puppet/puppet.conf /etc/puppet/puppet.conf

cp /etc/puppet/auth.conf /etc/puppet/auth.conf.orig
cp /tmp/puppet/auth.conf /etc/puppet/auth.conf

cp /etc/puppet/fileserver.conf /etc/puppet/fileserver.conf.orig
cp /tmp/puppet/fileserver.conf /etc/puppet/fileserver.conf

echo "Removing old puppet server certificates ..."
rm -rf /var/lib/puppet/ssl

echo "Generating new puppet master certificate ..."
puppet cert --generate pm

echo "Setting client certs autosigning ..."
echo "*" > /etc/puppet/autosign.conf

echo "Adjusting permissions for puppet config files ..."
chown -R puppet:puppet /etc/puppet/

echo "Configuring nginx to work with Passenger and puppet ..."
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.orig
cp /tmp/nginx/nginx.conf /etc/nginx/nginx.conf

cp /tmp/nginx/puppet.conf /etc/nginx/sites-available/puppet.conf
ln -s /etc/nginx/sites-available/puppet.conf /etc/nginx/sites-enabled/puppet.conf

echo "Disabling puppet master service ..."
update-rc.d -f puppetmaster remove

echo "Starting nginx with puppet master configured ..."
service nginx start

echo "Installing git ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install git >/dev/null

echo "Setting puppet repo ..."
mkdir -p /var/lib/puppet/.ssh
cp /tmp/ssh/config /var/lib/puppet/.ssh/config
chown puppet:puppet /var/lib/puppet/.ssh/config
cp /tmp/ssh/default_key /var/lib/puppet/.ssh/default_key
ssh-keyscan -H bitbucket.org >> /var/lib/puppet/.ssh/known_hosts
chown -R puppet:puppet /var/lib/puppet/.ssh/
chmod 600 /var/lib/puppet/.ssh/default_key

cp /tmp/ssh/config /root/.ssh/config
chown root:root /root/.ssh/config
cp /tmp/ssh/default_key /root/.ssh/default_key
ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
chown -R root:root /root/.ssh/
chmod 600 /root/.ssh/default_key

git clone git@bitbucket.org:pixelfields/puppet-master-example.git /opt/puppet-master-example
cp /tmp/git/post-merge /opt/puppet-master-example/.git/hooks/post-merge
chmod +x /opt/puppet-master-example/.git/hooks/post-merge
chown -R puppet:puppet /opt/puppet-master-example

echo "Creating symlinks to puppet manifests and modules directories ..."
rm -rf /etc/puppet/manifests/
rm -rf /etc/puppet/modules/

ln -s /opt/puppet-master-example/manifests /etc/puppet/manifests
ln -s /opt/puppet-master-example/modules /etc/puppet/modules
ln -s /opt/puppet-master-example/Puppetfile /etc/puppet/Puppetfile
ln -s /opt/puppet-master-example/files /etc/puppet/files
ln -s /opt/puppet-master-example/templates /var/lib/puppet/templates

echo "Installing trocla ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ruby-dev make gcc >/dev/null
gem install trocla

echo "Installing librarian-puppet ..."
gem install librarian-puppet
su -s /bin/bash puppet -c 'cd /etc/puppet && librarian-puppet install'

echo "Installing AWS cli ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install python-pip python-dev >/dev/null
pip install awscli

echo "Starting puppet agent ..."
puppet agent --enable
service puppet start
