#!/usr/bin/env sh

# This bootstraps Puppet on Debian
set -e

# Do the initial apt-get update
echo "Initial apt-get update..."
apt-get update >/dev/null

# Older versions of Debian don't have lsb_release by default, so 
# install that if we have to.
which lsb_release || apt-get --yes install lsb-release

# Load up the release information
DISTRIB_CODENAME=$(lsb_release -c -s)

REPO_DEB_URL="http://apt.puppetlabs.com/puppetlabs-release-${DISTRIB_CODENAME}.deb"

#--------------------------------------------------------------------
# NO TUNABLES BELOW THIS POINT
#--------------------------------------------------------------------
if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root." >&2
  exit 1
fi

# Install wget if we have to (some older Debian versions)
echo "Installing wget..."
apt-get --yes install wget >/dev/null

# Install the PuppetLabs repo
echo "Configuring PuppetLabs repo..."
repo_deb_path=$(mktemp)
wget --output-document="${repo_deb_path}" "${REPO_DEB_URL}" 2>/dev/null
dpkg -i "${repo_deb_path}" >/dev/null
rm "${repo_deb_path}"

apt-get update >/dev/null

echo "Setting timezone to Europe/Prague ..."
echo "Europe/Prague" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

echo "Running time synchronization ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ntpdate >/dev/null
ntpdate pool.ntp.org

echo "Installing ntp ..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install ntp >/dev/null

echo "Reconfiguring locales ..."
cp /etc/locale.gen /etc/locale.gen.orig
cat /dev/null > /etc/locale.gen
echo -e "en_US.UTF-8 UTF-8\ncs_CZ.UTF-8 UTF-8" > /etc/locale.gen

# Install Puppet
echo "Installing Puppet..."
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install puppet >/dev/null

echo "Puppet installed!"

echo "Stopping puppet client service ..."
service puppet stop > /dev/null

cp /etc/puppet/puppet.conf /etc/puppet/puppet.conf.orig
cp /tmp/puppet_agent.conf /etc/puppet/puppet.conf

chown puppet:puppet /etc/puppet/puppet.conf
chown -R puppet:puppet /var/lib/puppet/.aws/

echo "Disabling puppet service due broken systemd unit on debian jessie ..."
update-rc.d -f puppetmaster remove

echo "Enabling puppet agent ..."
puppet agent --enable

echo "Enabling puppet agent cron schedule ..."
chmod +x /etc/cron.d/puppet_agent

echo "Running initial puppet catalog for instance ..."
/usr/bin/puppet agent --onetime --no-daemonize --verbose --logdest syslog

