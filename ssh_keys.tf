# This will upload a new ssh key to Softlayer

resource "softlayer_ssh_key" "default_key" {
  name = "default_key"
  public_key = "${file(\"${var.sl_default_ssh_key_path}\")}"
}
